const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path')

module.exports = {
	entry: {
		main: './src/index.js'
		// pixi: './src/pixi.js'
	},
	output: {
		filename: '[name]-[contentHash].js',
		path: path.resolve(__dirname, 'dist')
	},
	plugins: [
		new HtmlWebpackPlugin({template: './src/template.html'}),
		new CopyPlugin([
			{ from: './src/pixi', to: './pixi' }
		]),
],
	module: {
		rules: [
			{
				test: /\.html$/,
				use: ['html-loader']
			},
			{
				test: /\.(png|jpg|svg)$/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[name]-[hash].[ext]',
						outputPath: 'img'
					}
				}
			}
		]
	}
}