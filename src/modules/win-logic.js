export default class WinLogic {
	constructor() {
		this.symbolsArr = [];
		this.winArr = [];
	}

	check(symbolsArr) {
		this.symbolsArr = symbolsArr;
		let maxNum = 0;
		let typeMax = 1;
		let num = 0;
		// check for all types
		for (let i = 1; i <= 9; i++) {
			num = this.howManyForType(i);
			if (num > maxNum) {
				maxNum = num;
				typeMax = i;
			}
		}
		if (maxNum >= 3) {
			this.pickType(typeMax);
		} else {
			this.clearWinArr();
		}
		return(this.winArr);
	}

	pickType(type) {
		let winArr = [];
		for (let i = 0; i < this.symbolsArr.length; i++) {
			winArr[i] = [];
			for (let j = 0; j < this.symbolsArr[i].length; j++) {
				if (this.symbolsArr[i][j] === type) {
					winArr[i][j] = 1;
				} else {
					winArr[i][j] = 0;
				}
			}
		}
		this.winArr = winArr;
	}

	isTypeInReel(reel, type) {
		for (let j = 0; j < this.symbolsArr[reel].length; j++) {
			if (this.symbolsArr[reel][j] === type) {
				return true;
			}
		}
	}

	howManyForType(type) {
		let num = 0;
		for (let i = 0; i < this.symbolsArr.length; i++) {
			if (this.isTypeInReel(i, type)) {
				num++;
			}
		}
		return num;
	}

	clearWinArr() {
		let winArr = [];
		for (let i = 0; i < this.symbolsArr.length; i++) {
			winArr[i] = [];
			for (let j = 0; j < this.symbolsArr[i].length; j++) {
				winArr[i][j] = 0;
			}
		}
		this.winArr = winArr;
	}
}