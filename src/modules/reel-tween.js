// tween code from demo example https://pixijs.io/examples/#/demos-advanced/slots.js
// more usefull than pixi-tween because of gentle 'backout' easing setting

export default class ReelTween {
	constructor() {
		this.tweening = [];
	}

	tweenTo(object, property, target, time, onchange, oncomplete) {
		const tween = {
			object,
			property,
			propertyBeginValue: object[property],
			target,
			easing: this.backout(0.5),
			time,
			change: onchange,
			complete: oncomplete,
			start: Date.now(),
		};
	
		this.tweening.push(tween);
		return tween;
	}

	updateTween(delta) {
		const now = Date.now();
		const remove = [];
		for (let i = 0; i < this.tweening.length; i++) {
			const t = this.tweening[i];
			const phase = Math.min(1, (now - t.start) / t.time);

			t.object[t.property] = this.lerp(t.propertyBeginValue, t.target, t.easing(phase));
			if (t.change) t.change(t);
			if (phase === 1) {
				t.object[t.property] = t.target;
				if (t.complete) t.complete(t);
				remove.push(t);
			}
		}
		for (let i = 0; i < remove.length; i++) {
			this.tweening.splice(this.tweening.indexOf(remove[i]), 1);
		}
	}

	lerp(a1, a2, t) {
		return a1 * (1 - t) + a2 * t;
	}

	backout(amount) {
		return t => (--t * t * ((amount + 1) * t + amount) + 1);
	}

}