import {createSprite} from './utils';

export default class WinSquares {
	constructor(reelNum, symbolsInReelNum, parentContainer) {
		this.reels = [];
		this.symbolsInReelNum = symbolsInReelNum;
		for (let i = 0; i < reelNum; i++) {
			this.reels[i] = {
				symbols: []
			}            
		}
		this.container = new PIXI.Container();
		parentContainer.addChild(this.container);
		this.container.x = 200;
		this.container.y = 180;
		this.symbolSize = 70;
		this.createWinSquares();
	}

	createWinSquares() {
		let size = this.symbolSize;
		let fieldW = size*5;
		let fieldH = size*3;
		this.winSquares = [];
		for (let i = 0; i < this.reels.length; i++) {
			this.winSquares[i] = [];
			for (let j = 0; j < this.symbolsInReelNum; j++) {
				const s = createSprite({
					x: i*size + size/2 - fieldW/2,
					y: j*size + size/2 - fieldH/2,
					img: 'win_square',
					cont: this.container,
					size: this.symbolSize
				});
				this.winSquares[i][j] = s;
			}
		}
		this.hide();
	}

	hide() {
		for (let i = 0; i < this.reels.length; i++) {
			for (let j = 0; j < this.symbolsInReelNum; j++) {
				let s = this.winSquares[i][j];
				s.visible = false;
			}
		}
	}

	show(winArr) {
		for (let i = 0; i < this.reels.length; i++) {
			for (let j = 0; j < this.symbolsInReelNum; j++) {
				let s = this.winSquares[i][j];
				if (winArr[i][j]) {
					s.visible = true;
				}
			}
		}
	}
}