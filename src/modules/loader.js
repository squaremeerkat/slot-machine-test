export default class Loader {
	constructor(onImagesLoaded) {
		this.loader = PIXI.loader;
		this.loadAssets();
		this.loader.once('complete', onImagesLoaded);
		this.loader.load();
	}
	
	loadAssets() {
		this.loader.add("01", require("../img/slot_01.png"));
		this.loader.add("02", require("../img/slot_02.png"));
		this.loader.add("03", require("../img/slot_03.png"));
		this.loader.add("04", require("../img/slot_04.png"));
		this.loader.add("05", require("../img/slot_05.png"));
		this.loader.add("06", require("../img/slot_06.png"));
		this.loader.add("07", require("../img/slot_07.png"));
		this.loader.add("08", require("../img/slot_08.png"));
		this.loader.add("09", require("../img/slot_09.png"));
		this.loader.add("win_square", require("../img/win_square.png"));
	}
}