import Loader from './loader';
import {createSprite, rInt, createTextButton} from './utils';
import Reels from './reels';
import ReelTween from './reel-tween';
import WinSquares from './win-squares';
import WinLogic from './win-logic';

export default class Game {
	constructor() {
		new Loader(this.onImagesLoaded.bind(this));
		this.app = new PIXI.Application({
			width: 400,
			height: 400,
			backgroundColor:0x4c7188
		});
		document.body.appendChild(this.app.view);
	}

	onImagesLoaded() {
		this.gameScene = new PIXI.Container();
		this.app.stage.addChild(this.gameScene);		
		this.reelTween = new ReelTween();
		this.reels = new Reels(5, 3, this.gameScene, this.reelTween, this.spinComplete.bind(this));
		this.winSquares = new WinSquares(5, 3, this.gameScene);
		this.winLogic = new WinLogic();

		createTextButton(200, 340, this.gameScene, 'SPIN', this.startSpin.bind(this));
		this.isSpinning = false;

		this.app.ticker.add((delta) => {
			this.reels.updateAnimation();
			this.reelTween.updateTween(delta);
		});
	}

	startSpin() {
		if (this.isSpinning) return;
		this.isSpinning = true;
		this.winSquares.hide();
		this.reels.startSpin();
	}

	spinComplete() {
		this.isSpinning = false;
		let symbolsArr = this.reels.collectSymbols();
		let winArr = this.winLogic.check(symbolsArr);
		this.winSquares.show(winArr);
	}
}