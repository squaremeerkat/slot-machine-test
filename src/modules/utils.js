export function createSprite({x, y, img, cont, size}) {
	const spr = new PIXI.Sprite(PIXI.loader.resources[img].texture);
	cont.addChild(spr);
	spr.x = x;
	spr.y = y;
	spr.anchor.set(0.5);
	let scaleX = size/spr.width;
	let scaleY = size/spr.height;
	spr.scale.set (scaleX, scaleY);
	return spr;
}

export function changeTexture(spr, img) {
	spr.texture = PIXI.utils.TextureCache[img];
}

export function rInt(a, b) {
	return Math.floor(Math.random() * (b + 1 - a)) + a;  
}

export function createTextButton(x, y, container, text, action) {
	let style = new PIXI.TextStyle({
		fontFamily: 'Arial',
		fontSize: 30,
		fontWeight: 'bold',
		fill: '#fff',
		stroke: '#000',
		strokeThickness: 3
	});
	
	let btn = new PIXI.Text(text, style);
	btn.x = x;
	btn.y = y;
	btn.anchor.set(0.5);
	container.addChild(btn);

	btn.buttonMode = true;
	btn.interactive = true;

	btn.on('click', action);
}