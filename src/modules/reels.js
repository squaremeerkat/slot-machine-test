import {createSprite, rInt, changeTexture} from './utils';

export default class Reels {
	constructor(reelNum, symbolsInReelNum, parentContainer, reelTween, spinComplete) {
		this.reels = [];
		this.symbolsInReelNum = symbolsInReelNum;
		this.parentContainer = parentContainer;
		this.reelTween = reelTween;
		this.spinComplete = spinComplete;
		for (let i = 0; i < reelNum; i++) {
			this.reels[i] = {
				position: 0,
				newPos: 0,
				symbols: []
			}            
		}
		this.container = new PIXI.Container();
		parentContainer.addChild(this.container);
		this.container.x = 200;
		this.container.y = 180;
		this.symbolSize = 70;

		this.createReels();
	}

	createReels() {
		let size = this.symbolSize;
		let fieldW = size*5;
		let fieldH = size*3;

		for (let i = 0; i < this.reels.length; i++) {
			for (let j = 0; j < this.symbolsInReelNum + 1; j++) {
				const s = {};
				let type = rInt(1, 9);
				s.type = type;
				s.sprite = createSprite({
					x: i*size + size/2 - fieldW/2,
					y: j*size + size/2 - fieldH/2,

					img: '0' + type,
					cont: this.container,
					size: size
				});
				s.symbolPos = j;
				this.reels[i].symbols[j] = s;
			}
		}

		this.createMask();
	}

	updateAnimation() {
		for (let i = 0; i < this.reels.length; i++) {
			let r = this.reels[i];
			r.prevPos = r.position;
			for (let j = 0; j < this.symbolsInReelNum + 1; j++) {
				let s = r.symbols[j];
				s.prevY = s.sprite.y;
				s.sprite.y = ((r.position + s.symbolPos) % (this.symbolsInReelNum+1)) * this.symbolSize
					- (this.symbolsInReelNum / 2 +.5) * this.symbolSize;
				if (s.sprite.y < s.prevY && s.prevY > 70+30) // only if under mask
				{
					// change type
					let type = rInt(1, 9);
					s.type = type;
					changeTexture(s.sprite, '0' + type);
				}
			}
		}
	}

	createMask() {
		let mask = new PIXI.Graphics();
		
		mask.clear();
		let w = this.symbolSize * this.reels.length;
		let h = this.symbolSize * this.symbolsInReelNum;
		mask.drawRect(this.container.x - w/2, this.container.y - h/2, w, h);
		
		this.container.mask = mask;
		this.parentContainer.addChild(mask);
	}

	startSpin() {
		for (let i = 0; i < this.reels.length; i++) {
			let r = this.reels[i];
			let target = Math.round(r.position + 10 + i * 5);
			let time = 2500 + i * 500;
			this.reelTween.tweenTo(r, 'position', target, time, null, i === this.reels.length - 1 ? this.spinComplete : null);
		}
	}

	collectSymbols() {
		let symbolsArr = [];
		for (let i = 0; i < this.reels.length; i++) {
			symbolsArr[i] = [];
			for (let j = 0; j < this.symbolsInReelNum + 1; j++) {
				let s = this.reels[i].symbols[j];
				let pos = 0;
				// -140, -70, 0, 70 - y coordinates in different positions
				if (s.sprite.y < -100) {
					continue; // symbol under mask
				} else if (s.sprite.y < -50) {
					pos = 0
				} else if (s.sprite.y < 10) {
					pos = 1
				} else {
					pos = 2
				}
				symbolsArr[i][pos] = s.type;
			}
		}
		return symbolsArr;
	}

}